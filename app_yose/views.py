from django.shortcuts import render,render_to_response
from django.http import HttpResponse, JsonResponse
import json
from django.core import serializers

# Create your views here.
def index(request):
  return render_to_response('index.djhtml')

def minesweeper(request):
  rows = range(1,9)
  cols = range(1,9)
  info = {'rows':rows,'cols':cols}  
  return render_to_response('minesweeper.djhtml',info)

def prime_factors(request):
  params = request.GET.getlist('number')
  print params
  resps = []
  for param in params:
    try:
      number = int(param)
      print param
      print number
      if number < -1:
        resps.append({'number':number,'error':" is not an integer > 1"})
      elif number < 1000000:
        mod = number
        desc = []
        while (mod >= 2):
          for i in range(2,1000000/2):
            if mod % i == 0:
              mod = mod / i
              desc.append(i)
              break
        resps.append({'number':number,'decomposition':desc})
      else:
        resps.append({'number':param,'error':"too big number (>1e6)"})
    except:
      resps.append({'number':param,'error':"not a number"})
  if len(resps) == 1:
    print "Salida1"
    print resps
    return JsonResponse(resps[0])
  else:
    print "Salida2"
    print resps
    return JsonResponse(resps,safe=False)


def prime_factors_ui(request):
  return render_to_response('prime_factors.djhtml')

def astroport(request):
  return render_to_response('astroport.djhtml')

def ping (request):
  return JsonResponse({'alive':True})
