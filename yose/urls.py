from django.conf.urls import patterns, include, url
from django.contrib import admin
from app_yose.views import *
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'yose.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', index),
    url(r'^minesweeper$', minesweeper),
    url(r'^primeFactors$', prime_factors),
    url(r'^primeFactors/ui$', prime_factors_ui),
    url(r'^astroport/', astroport),
    url(r'^ping/', ping),
)
